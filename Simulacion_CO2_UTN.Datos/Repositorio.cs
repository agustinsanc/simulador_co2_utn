﻿using System;
using System.Collections.Generic;

using Simulador_CO2_UTN.Modelo;

namespace Simulador_CO2_UTN.Datos
{
    public class Repositorio
    {
        private List<Marca> marcas;
        private List<Vehiculo> vehiculos;
        private List<Vehiculo> autos;
        private List<Vehiculo> camionetas;

        public Repositorio()
        {
            Iniciar();
        }

        public void Iniciar()
        {
            marcas = new List<Marca>();
            vehiculos = new List<Vehiculo>();
            autos = new List<Vehiculo>();
            camionetas = new List<Vehiculo>();

            marcas.Add(new Marca("FORD"));
            marcas.Add(new Marca("FIAT"));
            marcas.Add(new Marca("RENAULT"));
            marcas.Add(new Marca("TOYOTA"));
            marcas.Add(new Marca("VOLKSWAGEN"));
            marcas.Add(new Marca("CHEVROLET"));
            marcas.Add(new Marca("OTRAS"));

            autos.Add(new Vehiculo(marcas[0], "", 113.0, 114.5));
            autos.Add(new Vehiculo(marcas[1], "", 116.5, 132.5));
            autos.Add(new Vehiculo(marcas[2], "", 121.0, 131.25));
            autos.Add(new Vehiculo(marcas[3], "", 105.3, 110.3));
            autos.Add(new Vehiculo(marcas[4], "", 138.3, 149.3));
            autos.Add(new Vehiculo(marcas[5], "", 144.0, 149.5));
            autos.Add(new Vehiculo(marcas[6], "", 113.0, 135.0));

            camionetas.Add(new Vehiculo(marcas[0], "", 212.0, 230.0));
            camionetas.Add(new Vehiculo(marcas[1], "", 186.5, 195.5));
            camionetas.Add(new Vehiculo(marcas[2], "", 134.5, 143.5));
            camionetas.Add(new Vehiculo(marcas[3], "", 197.0, 207.5));
            camionetas.Add(new Vehiculo(marcas[4], "", 174.0, 182.0));
            camionetas.Add(new Vehiculo(marcas[5], "", 210.0, 222.05));
        }

        public void AgregarMarca(string nombre)
        {
            marcas.Add(new Marca(nombre));
        }

        public void AgregarVehiculo(Marca marca, string modelo, Double emisionMin, Double emisionMax)
        {
            vehiculos.Add(new Vehiculo(marca, modelo, emisionMin, emisionMax));
        }

        public List<Vehiculo> ObtenerAutos()
        {
            return autos;
        }

        public List<Vehiculo> ObtenerCamionetas()
        {
            return camionetas;
        }
    }
}
