﻿namespace Simulador_CO2_UTN.Modelo
{
    public class Marca
    {
        public string Nombre { get; }
        public Marca(string nombre)
        {
            this.Nombre = nombre;
        }
    }
}
