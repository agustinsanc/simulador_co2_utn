﻿namespace Simulador_CO2_UTN.Aplicacion.Interfaces
{
    public interface IVistaPrincipal
    {
        void MostrarInformeDiario(string informeDiario);
        void MostrarInformeMensual(string informeMensual);
        void MostrarInformeFinal(string informeFinal);
    }
}
