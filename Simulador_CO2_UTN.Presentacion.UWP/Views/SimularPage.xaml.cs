﻿using Simulador_CO2_UTN.Aplicacion;
using Simulador_CO2_UTN.Aplicacion.Interfaces;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;

namespace Simulador_CO2_UTN.Presentacion.UWP.Views
{
    public sealed partial class SimularPage : Page, INotifyPropertyChanged, IVistaPrincipal
    {
        private Controlador controlador;

        public SimularPage()
        {
            InitializeComponent();

            controlador = new Controlador(this);

            Texto_TitutoListaDias.Visibility = 0;
            Texto_TitutoListaMeses.Visibility = 0;
            Texto_TituloInformeFinal.Visibility = 0;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private void Boton_Simular_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Lista_InformesDiarios.Items.Clear();
            Lista_InformesMensuales.Items.Clear();

            if (CampoDeTexto_CantidadDeMeses.Text.Equals(""))
            {

            } else
            {
                Texto_TitutoListaDias.Visibility = 0;
                Texto_TitutoListaMeses.Visibility = 0;
                Texto_TituloInformeFinal.Visibility = 0;

                controlador.ConfigurarGeneradorDeNumerosAleatorios();
                int cantidadDeMeses = int.Parse(CampoDeTexto_CantidadDeMeses.Text);
                controlador.Simular(cantidadDeMeses);
            }
        }

        private void CampoDeTexto_Resultados_SelectionChanged(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }

        // Informacion de cada dia
        public void MostrarInformeDiario(string informe)
        {
            Lista_InformesDiarios.Items.Add(informe);
        }

        public void MostrarInformeMensual(string informe)
        {
            Lista_InformesMensuales.Items.Add(informe);
        }

        public void MostrarInformeFinal(string informe)
        {
            CajaTexto_InformeFinal.Text = informe;
        }
    }
}
