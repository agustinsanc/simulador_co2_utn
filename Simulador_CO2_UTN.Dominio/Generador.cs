﻿namespace Simulador_CO2_UTN.Modelo
{
    public class Generador
    {
        private long a;
        private long c;
        private long m;
        private long n;

        public Generador(long a, long c, long m, long semilla)
        {
            this.a = a;
            this.c = c;
            this.m = m;
            n = semilla;
        }

        public double Generar()
        {
            n = (a * n + c) % m;
            
            return (double) n / m; 
        }
    }
}
