﻿namespace Simulador_CO2_UTN.Modelo
{
    public class Vehiculo
    {
        public Marca Marca { get; }
        public string Modelo { get; }
        public double EmisionMin { get; }
        public double EmisionMax { get; }

        public Vehiculo(Marca marca, string modelo, double emisionMin, double emisionMax)
        {
            Marca = marca;
            Modelo = modelo;
            EmisionMin = emisionMin;
            EmisionMax = emisionMax;
        }
    }
}
