﻿using Simulador_CO2_UTN.Aplicacion.Interfaces;
using Simulador_CO2_UTN.Datos;
using Simulador_CO2_UTN.Modelo;
using System;
using System.Collections.Generic;

namespace Simulador_CO2_UTN.Aplicacion
{
    public class Controlador
    {
        public Simulador simulador;
        public Repositorio repositorio;
        public IVistaPrincipal vista;

        public Controlador(IVistaPrincipal vista)
        {
            simulador = new Simulador();
            repositorio = new Repositorio();
            this.vista = vista;
        }

        public void ConfigurarGeneradorDeNumerosAleatorios()
        {
            simulador.ConfigurarGenerador(4309, 2311, 6031);
        }

        public void Simular(int cantidadDeMeses)
        {
            // Obtenemos los datos de los vehiculos del repositorio de datos
            List<Vehiculo> autos = repositorio.ObtenerAutos();
            List<Vehiculo> camionetas = repositorio.ObtenerCamionetas();

            int cantidadDeDias = 20;

            double acumulacionDePromediosMensualesCO2_Autos = 0;
            double acumulacionDePromediosMensualesCO2_Camionetas = 0;

            // Iteramos mes a mes
            for (int mes = 1; mes <= cantidadDeMeses; mes++)
            {
                double acumulacionDePromediosDiariosCO2_Autos = 0;
                double acumulacionDePromediosDiariosCO2_Camionetas = 0;

                // Iteramos dia por dia
                for (int dia = 1; dia <= cantidadDeDias; dia++)
                {
                    int cantidadAutos = 0, cantidadCamionetas = 0;
                    double totalDiarioCO2_Autos = 0, totalDiarioCO2_Camionetas = 0;

                    int cantidadDeVehiculos = simulador.DistribucionNormal(250, 50);

                    // Iteramos vehiculo por vehiculo
                    for (int vehiculo = 1; vehiculo <= cantidadDeVehiculos; vehiculo++)
                    {
                        double tipoDeVehiculo = simulador.GenerarNumeroAleatorio();

                        // Comprobamos si el tipo de vehiculo es auto o camioneta
                        if (simulador.DistribucionBinomial(tipoDeVehiculo, 0.7400))
                        {
                            cantidadAutos++;
                            double marcaDeAuto = simulador.GenerarNumeroAleatorio();

                            // Comprobamos la marca del auto para saber el promedio de CO2 que emite
                            if (simulador.DistribucionBinomial(marcaDeAuto, 0.0855))
                            {
                                totalDiarioCO2_Autos += simulador.DistribucionUniforme(autos[0].EmisionMin, autos[0].EmisionMax, simulador.GenerarNumeroAleatorio());
                            }
                            else
                            {
                                if (simulador.DistribucionBinomial(marcaDeAuto, 0.3480))
                                {
                                    totalDiarioCO2_Autos += simulador.DistribucionUniforme(autos[1].EmisionMin, autos[1].EmisionMax, simulador.GenerarNumeroAleatorio());
                                }
                                else
                                {
                                    if (simulador.DistribucionBinomial(marcaDeAuto, 0.4763))
                                    {
                                        totalDiarioCO2_Autos += simulador.DistribucionUniforme(autos[2].EmisionMin, autos[2].EmisionMax, simulador.GenerarNumeroAleatorio());
                                    }
                                    else
                                    {
                                        if (simulador.DistribucionBinomial(marcaDeAuto, 0.5030))
                                        {
                                            totalDiarioCO2_Autos += simulador.DistribucionUniforme(autos[3].EmisionMin, autos[3].EmisionMax, simulador.GenerarNumeroAleatorio());
                                        }
                                        else
                                        {
                                            if (simulador.DistribucionBinomial(marcaDeAuto, 0.6527))
                                            {
                                                totalDiarioCO2_Autos += simulador.DistribucionUniforme(autos[4].EmisionMin, autos[4].EmisionMax, simulador.GenerarNumeroAleatorio());
                                            }
                                            else
                                            {
                                                if (simulador.DistribucionBinomial(marcaDeAuto, 0.8927))
                                                {
                                                    totalDiarioCO2_Autos += simulador.DistribucionUniforme(autos[5].EmisionMin, autos[5].EmisionMax, simulador.GenerarNumeroAleatorio());
                                                }
                                                else
                                                {
                                                    totalDiarioCO2_Autos += simulador.DistribucionUniforme(autos[6].EmisionMin, autos[6].EmisionMax, simulador.GenerarNumeroAleatorio());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            cantidadCamionetas++;
                            double marcaDeCamioneta = simulador.GenerarNumeroAleatorio();

                            // Comprobamos la marca de la camioneta para saber el promedio de CO2 que emite
                            if (simulador.DistribucionBinomial(marcaDeCamioneta, 0.1718))
                            {
                                totalDiarioCO2_Camionetas += simulador.DistribucionUniforme(camionetas[0].EmisionMin, camionetas[0].EmisionMax, simulador.GenerarNumeroAleatorio());
                            }
                            else
                            {
                                if (simulador.DistribucionBinomial(marcaDeCamioneta, 0.3622))
                                {
                                    totalDiarioCO2_Camionetas += simulador.DistribucionUniforme(camionetas[1].EmisionMin, camionetas[1].EmisionMax, simulador.GenerarNumeroAleatorio());
                                }
                                else
                                {
                                    if (simulador.DistribucionBinomial(marcaDeCamioneta, 0.5844))
                                    {
                                        totalDiarioCO2_Camionetas += simulador.DistribucionUniforme(camionetas[2].EmisionMin, camionetas[2].EmisionMax, simulador.GenerarNumeroAleatorio());
                                    }
                                    else
                                    {
                                        if (simulador.DistribucionBinomial(marcaDeCamioneta, 0.7907))
                                        {
                                            totalDiarioCO2_Camionetas += simulador.DistribucionUniforme(camionetas[3].EmisionMin, camionetas[3].EmisionMax, simulador.GenerarNumeroAleatorio());
                                        }
                                        else
                                        {
                                            if (simulador.DistribucionBinomial(marcaDeCamioneta, 0.9018))
                                            {
                                                totalDiarioCO2_Camionetas += simulador.DistribucionUniforme(camionetas[4].EmisionMin, camionetas[4].EmisionMax, simulador.GenerarNumeroAleatorio());
                                            }
                                            else
                                            {
                                                totalDiarioCO2_Camionetas += simulador.DistribucionUniforme(camionetas[5].EmisionMin, camionetas[5].EmisionMax, simulador.GenerarNumeroAleatorio());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    double emisionPromedioDiariaCO2_Autos = totalDiarioCO2_Autos / cantidadAutos;
                    double emisionPromedioDiarioCO2_Camionetas = totalDiarioCO2_Camionetas / cantidadCamionetas;

                    acumulacionDePromediosDiariosCO2_Autos += emisionPromedioDiariaCO2_Autos;
                    acumulacionDePromediosDiariosCO2_Camionetas += emisionPromedioDiarioCO2_Camionetas;

                    vista.MostrarInformeDiario(String.Format("Mes: {0}. Dia: {1}. Autos: {2}, EP: {3:0.00} [gr/km]. Camionetas: {4}. EP: {5:0.00} [gr/km].", 
                        mes, dia, cantidadAutos, emisionPromedioDiariaCO2_Autos, cantidadCamionetas, emisionPromedioDiarioCO2_Camionetas));
                }

                double emisionPromedioMensualCO2_Autos = acumulacionDePromediosDiariosCO2_Autos / cantidadDeDias;
                double emisionPromedioMensualCO2_Camionetas = acumulacionDePromediosDiariosCO2_Camionetas / cantidadDeDias;

                acumulacionDePromediosMensualesCO2_Autos += emisionPromedioMensualCO2_Autos;
                acumulacionDePromediosMensualesCO2_Camionetas += emisionPromedioMensualCO2_Camionetas;
                vista.MostrarInformeMensual(String.Format("Mes {0:00}. EP Autos: {1:0.00} [gr/km]. EP Camionetas: {2:0.00} [gr/km].", mes, emisionPromedioMensualCO2_Autos, emisionPromedioMensualCO2_Camionetas));
            }

            double emisionPromedioTotalCO2_Autos = acumulacionDePromediosMensualesCO2_Autos / cantidadDeMeses;
            double emisionPromedioTotalCO2_Camionetas = acumulacionDePromediosMensualesCO2_Camionetas / cantidadDeMeses;

            string informeFinal = "";

            // Comprobamos si en los autos se cumple el estandar 2015, el estandar 2021 o ninguno
            informeFinal += "Autos \n";

            if (emisionPromedioTotalCO2_Autos <= 130.0)
                if (emisionPromedioTotalCO2_Autos <= 95.0) informeFinal += "Se cumple el estandar del año 2021 (<= 95.0 [gr/km]). \n";
                else informeFinal += "Se cumple el estandar del año 2015 (<= 130.0 [gr/km]). \n";
            else
            {
                informeFinal += String.Format("No se cumplio ningun estandar. EP = {0:0.00} [gr/km]. \n", emisionPromedioTotalCO2_Autos);
                informeFinal += String.Format("Diferencia respecto al estandar 2015: {0:0.00} [gr/km]. \n", (emisionPromedioTotalCO2_Autos - 130.0));
                informeFinal += String.Format("Diferencia respecto al estandar 2021: {0:0.00} [gr/km]. \n", (emisionPromedioTotalCO2_Camionetas - 95.0));
            }


            // Comprobamos si en las camionetas se cumple el estandar 2015, el estandar 2021 o ninguno
            informeFinal += "\nCamionetas \n";

            if (emisionPromedioTotalCO2_Camionetas <= 170.0)
                if (emisionPromedioTotalCO2_Camionetas <= 147.0) informeFinal += "Se cumple el estandar del año 2021 (<= 147.0 [gr/km]). \n";
                else informeFinal += "Se cumple el estandar del año 2015 (<= 170.0 [gr/km]). \n";
            else
            {
                informeFinal += String.Format("No se cumplio ningun estandar. EP = {0:0.00} [gr/km]. \n", emisionPromedioTotalCO2_Camionetas);
                informeFinal += String.Format("Diferencia respecto al estandar 2015: {0:0.00} [gr/km]. \n", (emisionPromedioTotalCO2_Camionetas - 170.0));
                informeFinal += String.Format("Diferencia respecto al estandar 2021: {0:0.00} [gr/km]. \n", (emisionPromedioTotalCO2_Camionetas - 147.0));
            }

            vista.MostrarInformeFinal(informeFinal);
        }
    }
}