﻿using System;

namespace Simulador_CO2_UTN.Modelo
{
    public class Simulador
    {
        private Generador generadorDeNumerosAleatorios;

        public void ConfigurarGenerador(long a, long c, long m)
        {
            long semilla = new Random().Next();
            generadorDeNumerosAleatorios = new Generador(a, c, m, semilla);
        }

        public double DistribucionUniforme(double limiteInferior, double limiteSuperior, double numeroAleatorio)
        {
            return limiteInferior + (limiteSuperior - limiteInferior) * numeroAleatorio;
        }

        public int DistribucionNormal(int media, int desvio)
        {
            double sumatoria = 0;

            for (int i = 1; i <= 12; i++)
            {
                double numeroAleatorio = generadorDeNumerosAleatorios.Generar();
                sumatoria += numeroAleatorio;
            }

            return (int)(media + desvio * (sumatoria - 6));
        }

        public bool DistribucionBinomial(double numeroAleatorio, double probabilidadDeExito)
        {
            return numeroAleatorio <= probabilidadDeExito;
        }

        public double GenerarNumeroAleatorio()
        {
            return generadorDeNumerosAleatorios.Generar();
        }
    }
}
